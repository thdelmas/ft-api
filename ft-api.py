#!/usr/bin/env python3                                                          
# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    happy.py                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: thdelmas <thdelmas@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/02/10 02:34:14 by thdelmas          #+#    #+#              #
#    Updated: 2020/02/10 03:57:20 by thdelmas         ###   ########.fr        #
#                                                                              #
#    Interpreter: GNU bash, version 3.2.57(1)-release (x86_64-apple-darwin18)  #
#                                                                              #
#    Usage: happy.py                                                           #
#    Version: 0.0.1                                                            #
#                                                                              #
# **************************************************************************** #

import requests
import os
import json
import time
import sys

def connect_happy():
    print('Happy 42')
    url = 'https://api.intra.42.fr/oauth/token'
    uid = os.getenv('API42_UID')
    if not uid:
        print('API42_UID not set')
        exit(1)
    secret = os.getenv('API42_SECRET')
    if not secret:
        print('API42_SECRET not set')
        exit(1)
    data = {
            'grant_type':'client_credentials',
            'client_id':uid,
            'client_secret':secret
            }
    answer = requests.post(url, data)
    token = answer.json()['access_token']
    print('API42_TOKEN: ' + token)
    return token

def get_api_ft(token, params={'per_page': '100'}, endpoint=""):
    if "" == endpoint:
        endpoint = input("Enter some endpoint: ")
    if not endpoint:
        return 
    response = requests.get(
        "https://api.intra.42.fr" + endpoint,
        headers={'Authorization': 'Bearer ' + token},
        )
    try:
        nb_page = int(response.headers['X-Total']) / int(response.headers['X-Per-Page'])
        if int(response.headers['X-Total']) % int(response.headers['X-Per-Page']):
            nb_page += 1
    except:
        nb_page = 1
    page = 0
    while page < int(nb_page):
        time.sleep(0.25)
        response = requests.get(
            "https://api.intra.42.fr" + endpoint,
            params={'per_page': '100', 'page': str(page + 1)},
            headers={'Authorization': 'Bearer ' + token},
            )
        try:
            data = response.content
            data2 = json.loads(data)
            print(response.headers)
            print(json.dumps(data2, indent=2))
            page += 1
        except:
            return

if __name__ == '__main__':
    token = connect_happy()
    if 1 >= len(sys.argv):
        while True:
            if "" == get_api_ft(token):
                exit(0)
    else:
        get_api_ft(token, endpoint=sys.argv[1])

